#include<iostream>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/mman.h>
#include<fcntl.h>
#include<unistd.h>
#include"DatosMemCompartida.h"

int main(){
	int fdbot;
	DatosMemCompartida* pundatos;
	char* proyeccion;
	
	fdbot=open("/tmp/datosBot.txt", O_RDWR);

	proyeccion=(char*)mmap(NULL, sizeof(*(pundatos)), PROT_WRITE|PROT_READ, MAP_SHARED, fdbot, 0);

	close(fdbot);
	
	pundatos=(DatosMemCompartida*)proyeccion;

	while(1){
		usleep(25000);
		if(((pundatos->raqueta1.y1+pundatos->raqueta1.y2)/2) < pundatos->esfera.centro.y) pundatos->accion1=1;
		else if(((pundatos->raqueta1.y1+pundatos->raqueta1.y2)/2) > pundatos->esfera.centro.y) pundatos->accion1=-1;
		else pundatos->accion1=0;

		if(((pundatos->raqueta2.y1+pundatos->raqueta2.y2)/2) < pundatos->esfera.centro.y) pundatos->accion2=1;
                else if(((pundatos->raqueta2.y1+pundatos->raqueta2.y2)/2) > pundatos->esfera.centro.y) pundatos->accion2=-1;
                else pundatos->accion2=0;

	}

	munmap(proyeccion, sizeof(*(pundatos)));
}
