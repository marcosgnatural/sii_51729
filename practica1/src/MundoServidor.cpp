// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*char* proyeccion;
static int contador=0;
static int estadocontrol=0;*/

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//se cierra la tuberia en Mundo.cpp
	int clog=close(fdlog);
	if(clog==-1){perror("Error al cerrar la tuberia en Mundo.cpp");}

	int cser=close(fdser);
	if(clog==-1){perror("Error al cerrar la tuberia en Mundo.cpp");}
	
	
	//se cierra la proyeccion de memoria
	//munmap(proyeccion, sizeof(datos));

	//se cierran las tuberias de coordenadas y de teclas en mundoServidor
	write(fd_coordenadas, cadenaFin, strlen(cadenaFin)+1);
	close(fd_coordenadas);
	 
	close(fd_teclas);
}


void* hilo_comandos(void* d)
{
	CMundoServidor* p=(CMundoServidor*) d;
	p->RecibeComandosJugador();
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		//esfera.centro.x=0;
		//esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=0.15;
		//esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

	char cad[200];
	sprintf (cad,"Punto del jugador 1, lleva %d  puntos",puntos1);
	int wt=write(fdlog,cad,strlen(cad)+1);
	if (wt == -1){perror("Error de escritura en logger");}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		//esfera.centro.x=0;
		//esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-0.15;
		//esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

	char cad[200];
	sprintf(cad, "Punto del jugador 2, lleva %d puntos", puntos2);
	int wt=write(fdlog, cad, strlen(cad)+1);
	if(wt==-1){perror("Error de escritura en el logger");}
	}

	//el juego finaliza cuando uno de los jugadores obtiene 3 puntos
	if (puntos1 == 3)
	{
	char cad[200];
	sprintf(cad,"Jugador 1 ha ganado la partida");
	int wt=write(fdlog,cad,strlen(cad)+1);
	estadocontrol=5;
	}
	if (estadocontrol==5) {estadocontrol=0; exit (0);}

	if (puntos2 == 3)
        { 
        char cad[200]; 
        sprintf(cad,"Jugador 2 ha ganado la partida");
        int wt=write(fdlog,cad,strlen(cad)+1);
        estadocontrol=5;
        }
        if (estadocontrol==5) {estadocontrol=0; exit (0);}
	

	pundatos->esfera=esfera;
	pundatos->raqueta1=jugador1;
	pundatos->raqueta2=jugador2;

	/*logica del bot
	switch(pundatos->accion1)
	{
		case 1: {OnKeyboardDown('o',0,0); break;}
		case -1: {OnKeyboardDown('l',0,0); break;}
		case 0: break;
	}

	if(contador>=450)estadocontrol=1;
	if(estadocontrol)
	{
		switch(pundatos->accion2)
        	{
                	case 1: {OnKeyboardDown('o',0,0); break;}
                	case -1: {OnKeyboardDown('l',0,0); break;}
                	case 0: break;
		}
	}*/


	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2);
	
	//se envian los datos por la tuberia
	int escr=write(fd_coordenadas, cad, strlen(cad)+1);
	if(escr==1){
		perror("Error al escribir en la tuberia coord\n");
		exit(1);
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-0.1;break;
	case 'w':jugador1.velocidad.y=0.1;break;
	case 'l':jugador2.velocidad.y=-0.1;break;
	case 'o':jugador2.velocidad.y=0.1;break;

	}
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//se abre la tuberia en mundo.cpp
	fdlog=open("/tmp/loggerfifo", O_WRONLY);
	if(fdlog==-1){perror("Error en la apretura de logger en Mundo.cpp");}
		
	
	//fdbot=open("/tmp/datosBot.txt", O_RDWR|O_CREAT|O_TRUNC, 0777);
	//if(fdbot==-1){perror("Error en la apertura del fichero del bot");}
	//int wbot=write(fdbot, &datos, sizeof(datos));
	//if(wbot==-1){perror("Error en la escritura del bot en Datos");}
	//proyeccion=(char*)mmap(NULL, sizeof(datos), PROT_WRITE|PROT_READ, MAP_SHARED, fdbot, 0);
	//int cbot=close(fdbot);
	//if(cbot==-1){perror("Error al cerrar el fichero bot");}
	
	//pundatos=(DatosMemCompartida*)proyeccion;

//	pundatos->accion1=0;
//	pundatos->accion2=0;

	//se abren las tuberias de coordenadas y de teclas
	fd_coordenadas=open("/tmp/tuberiaCoordenadas", O_WRONLY);
	if(fd_coordenadas==-1){
		perror("Error al abrir la tuberia coord");
		exit(1);
	}


	fd_teclas=open("/tmp/tuberiaTeclas", O_RDONLY);
	if(fd_teclas==-1){
		perror("Error al abrir la tuberia teclas");
		exit(1);
	}

	pthread_create(&thid1, NULL, hilo_comandos, this);
}

void CMundoServidor:: RecibeComandosJugador()
{
	while(1){
		usleep(10);
		char cad[100];
		read(fd_teclas, cad, sizeof(cad));
		unsigned char key;
	
		sscanf(cad, "%c", &key);
		if(cad[0]=='F')exit(1);
		else 
			if(key=='s')jugador1.velocidad.y=-4;
			if(key=='w')jugador1.velocidad.y=4;
			if(key=='l')jugador2.velocidad.y=-4;
			if(key=='o')jugador2.velocidad.y=4;
			
	}


}
