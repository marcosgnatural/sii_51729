#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<fcntl.h>
#include<unistd.h>


int main (int argc, char *argv[])
{
	int rd;	

	int fifo=mkfifo("/tmp/loggerfifo",0777);
	if(fifo==-1){perror("Error al crear la tuberia");}

	int fdlog=open("/tmp/loggerfifo",O_RDONLY);
	if (fdlog==-1){perror("Error al abrir la tuberia en logger");}

	char buf[500];
	while (read(fdlog,buf,sizeof(buf)))
	{
		printf ("%s\n", buf);
		
		if (rd==-1 || buf[0]== 'E'){
			printf("Tenis cerrado, Cerrando logger\n");
			break;
		}
	}
	
	int clogger= close(fdlog);
	if (clogger == -1){perror("Error al cerrar la tuberia en logger");}

	unlink("/tmp/loggerfifo");
	
	return 0;
}
